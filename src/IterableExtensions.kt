fun <T, Q> Iterable<T>.ifContains(t: T, block: () -> Q): Q? { return if (this.contains(t)) block() else null }
