import org.json.JSONArray
import org.json.JSONObject

inline fun <reified T: Enum<T>> JSONObject.getEnums(): List<Enum<T>> {
    val jArrFromPluralizedEnumClassName: JSONArray? = this.optJSONArray("${T::class.simpleName?.toLowerCase()}s")
    return jArrFromPluralizedEnumClassName?.map(Any::toString)?.mapNotNull { it.toEnum<T>() } ?: listOf()
}

fun JSONObject.getJSONObjects(): List<JSONObject> {
    return this.optJSONArray(getFirstKey())?.map { it as JSONObject } ?: listOf(this.optJSONObject(getFirstKey()))
}

fun JSONObject.get(index: Int): String? {
    val keys = this.keys()
    return repeatAndGetLast(index + 1) { keys?.next() }
}

fun JSONObject.getFirstKey(): String? = get(0)
fun JSONObject.getLastKey(): String? = get(length())