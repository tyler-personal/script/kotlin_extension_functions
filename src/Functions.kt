fun <T> tryOrNull(block: () -> T?): T? = try { block() } catch (e: Exception) { null }
fun <T> repeat(times: Int, block: () -> T) { 0.until(times).forEach { block() } }

fun <T> repeatAndGetLast(times: Int, block: () -> T): T? {
    repeat(times-1, block)
    return if (times > 0) block() else null
}