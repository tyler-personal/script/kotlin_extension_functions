import kotlin.reflect.full.memberProperties

fun <T> T.tap(block: (T) -> Any?): T { block(this); return this }
fun <T, Q> T.change(block: (T) -> Q): Q = block(this)

inline val <reified T: Any> T.propertiesMap
    get() = T::class.memberProperties.map { it.name to it.getter.call(this).toString() }.toMap()



