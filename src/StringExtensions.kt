fun String.last(num: Int) = this.substring(this.length-num)
fun String.untilLast(num: Int) = this.substring(0.rangeTo(this.length-num))
fun String.first(num: Int) = this.substring(0.until(num))
fun String.safeURL() = this.replace("\"", "%22").replace(" ", "%20") // TODO: Make this function convert more characters

inline fun <reified T: Enum<T>> String.toEnum(): Enum<T>? {
    val enums = listOf(this, this.toUpperCase(), this.toLowerCase())
    return enums.mapNotNull { tryOrNull { enumValueOf<T>(it) } }.firstOrNull()
}
